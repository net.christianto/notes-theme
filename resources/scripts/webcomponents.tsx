
import r2wc from "@r2wc/react-to-web-component";
import { webComponents } from "../components/index";
import { toKebabCase } from "./utils/to-kebab-case";

webComponents.forEach(element => {
  customElements.define(
    toKebabCase(element.component.displayName ?? element.component.name),
    r2wc(
      element.component, {
      props: element.props
    }));
});
