<?php

/**
 * Generate a WebComponent that are connected with React.
 */

 use Illuminate\Support\Str;

$dom = new DomDocument();
$nameNode = $dom->createElement(Str::kebab($name));

if ($class) {
  $nameNode->setAttribute('class-names', $class);
}

foreach ($attributes as $name => $val) {
  $nameNode->setAttribute(Str::kebab($name), json_encode($val));
}

echo $dom->saveHTML($nameNode);
