<header class="banner">
  <x-react name="NoteNavbar" :menus="wp_get_nav_menu_items('primary_navigation') ?: []" :branding="['site'=>$siteName, 'link'=> home_url('/')]" />
</header>
