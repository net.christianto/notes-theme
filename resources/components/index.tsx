import React from "react";
import NoteNavbar from "./navbar/navbar";

export interface WebComponents<Props extends object = any> {
  component: React.ComponentType<Props>,
  props: Props
}

export const webComponents: WebComponents[] = [
  {
    component: NoteNavbar,
    props: {
      menus: "json",
      branding: "json"
    }
  }
]


