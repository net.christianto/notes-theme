import r2wc from '@r2wc/react-to-web-component'
import React, { useId } from 'react'
import { Button, Col, Container, Form, Navbar, Row } from 'react-bootstrap';

export interface NoteNavbarProps {
  menus: any[],
  branding: { link: string, site: string },
  hasSearch?: boolean
}


function NoteNavbar({ branding }: NoteNavbarProps) {
  const collapseTargetId = useId();

  return (
    <>
      <Navbar expand="lg" variant='dark'>
        <Container>
          <Navbar.Brand href={branding.link}>{branding.site}</Navbar.Brand>
          <Navbar.Toggle aria-controls={collapseTargetId} />
          <Navbar.Collapse id={collapseTargetId}>
            <Form action={branding.link} className='flex-grow-1'>
              <Row className='g-0'>
                <Col xs="auto" className='flex-grow-1'>
                  <Form.Control
                    type="text"
                    size='lg'
                    name='s'
                    placeholder="Search"
                    className="mr-sm-2"
                  />
                </Col>
                <Col xs="auto">
                  <Button size='lg' type="submit" variant='secondary'>Search</Button>
                </Col>
              </Row>
            </Form>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  )
}

NoteNavbar.displayName = 'NoteNavbar';

export default NoteNavbar;
