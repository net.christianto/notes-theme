/**
 * Compiler configuration
 *
 * @see {@link https://roots.io/docs/sage sage documentation}
 * @see {@link https://bud.js.org/guides/configure bud.js configuration guide}
 *
 * @type {import('@roots/bud').Config}
 */
export default async (app) => {
  /**
   * Application assets & entrypoints
   *
   * @see {@link https://bud.js.org/docs/bud.entry}
   * @see {@link https://bud.js.org/docs/bud.assets}
   */
  app
    .entry('app', ['@scripts/app', '@styles/app.scss'])
    .entry('editor', ['@scripts/editor', '@styles/editor.scss'])
    .assets(['images']);

  /**
   * Set public path
   *
   * @see {@link https://bud.js.org/docs/bud.setPublicPath}
   */
  app.setPublicPath('/app/themes/sage/public/');

  /**
   * Development server settings
   *
   * @see {@link https://bud.js.org/docs/bud.setUrl}
   * @see {@link https://bud.js.org/docs/bud.setProxyUrl}
   * @see {@link https://bud.js.org/docs/bud.watch}
   */
  app
    .setUrl('http://localhost:3000')
    .watch(['resources', 'app']);
  app.react.refresh.enable();

  /**
   * Generate WordPress `theme.json`
   *
   * @note This overwrites `theme.json` on every build.
   *
   * @see {@link https://bud.js.org/extensions/sage/theme.json}
   * @see {@link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-json}
   */
  app.wpjson
    .setSettings({
      color: {
        custom: false,
        customDuotone: false,
        customGradient: false,
        defaultDuotone: false,
        defaultGradients: false,
        defaultPalette: false,
        duotone: [],
        palette: [
          {
            name: 'Wandering White',
            slug: 'wandering-white',
            color: '#d0d3d4'
          },
          {
            name: 'Gloomy Gray',
            slug: 'gloomy-gray',
            color: '#1d252d'
          },
          {
            name: 'Restless Red',
            slug: 'restless-red',
            color: '#ee2737'
          },
          {
            name: 'Optimistic Orange',
            slug: 'optimistic-orange',
            color: '#ff6a14'
          },
          {
            name: 'Paranormal Pink',
            slug: 'paranormal-pink',
            color: '#df1995'
          },
          {
            name: 'Gladly Green',
            slug: 'gladly-green',
            color: '#97d700'
          },
          {
            name: 'Clinical Cyan',
            slug: 'clinical-cyan',
            color: '#2cccd3'
          },
          {
            name: 'Persuasive Purple',
            slug: 'persuasive-purple',
            color: '#753bbd'
          },
        ]
      },
      custom: {
        spacing: {},
        typography: {
          'font-size': {},
          'line-height': {},
        },
      },
      spacing: {
        padding: true,
        units: ['px', '%', 'em', 'rem', 'vw', 'vh'],
      },
      typography: {
        customFontSize: false,
      },
    }).enable();
};
