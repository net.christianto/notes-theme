<?php

namespace App\View\Components;

use Illuminate\View\Component;

class React extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(public string $name, public ?string $class = "")
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.react', [
            'name' => $this->name,
            'class' => $this->class
        ]);
    }
}
